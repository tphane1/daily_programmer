'''
From inputs such as '3d6, 1d20, 5d4' generate the output of 
the rolled dice sums. The first digit is the number of rolls,
the letter 'd' simply stands for dice, and the second (last) 
digit is the number of sides on the rolled die. 3d6 meaning rolling a 
6 sided die 3 times. 

ie.
3d6 = "{7} 4 + 2 + 1"
5d4 = "{12} 2 + 2 + 1 + 4 + 3"
'''

from random import randint
roll_list = []
roll_sum = 0

# Get the roll from user
roll = input("Enter your roll (ex. 2d6): ").split('d')


# Generate the list of rolls
for i in range(0, int(roll[0])):
    rand_num = randint(1, int(roll[1]))
    roll_list.append(rand_num)

# Calculate sum of roll
for i in roll_list:
    roll_sum = roll_sum + i

print("Roll is {}: {}".format(roll_sum, roll_list))
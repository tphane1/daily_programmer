''' 
Check strings follow 'ie' rule
- If "ei" appears in a word, it must immediately follow "c"
- If "ie" appears in a word, it must not immediately follow "c"
- A word also follows the rule if neither "ei" nor "ie" appears 
anywhere in the word

EXAMPLES:
check("a") => true
check("zombie") => true
check("transceiver") => true
check("veil") => false
check("icier") => false

BONUS:
How many words in 'https://norvig.com/ngrams/enable1.txt' word list 
are exceptions to the rule? 
(The answer is 4 digits long and the digits add up to 18.)
'''

# Rule checking function
# Nested If/Else nightmare that I should split up...
# But Im not going to! =)
def check(string):
    if 'ie' in string:
        if 'cie' in string:
            return False
        else:
            return True
    elif 'ei' in string:
        if 'cei' in string:
            string.replace("cei", "")
            if 'ei' in string:
                return True
            else:
                return False
        else:
            return False
    else:
        return True

# Testing examples
print(check("a"))
print(check("zombie"))
print(check("transceiver"))
print(check("veil"))
print(check("icier"))


# BONUS
# Open word-list, strip newline character
with open('res/enable1.txt') as f:
    enable_list = [line.rstrip('\n') for line in f]

# Checking for rule exceptions
num_exceptions = 0
for i in enable_list:
    if check(i) == True:
        continue
    else:
        num_exceptions += 1

print(num_exceptions)


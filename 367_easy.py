'''
Calculate the derangement of given values 'n'
    !1 -> 0 (You always have {1})
    !2 -> 1 (You always have {2,1})
    !3 -> 2 (You always have{2,3,1} and {3,1,2})

https://en.wikipedia.org/wiki/Derangement
!n = (n-1)(!(n-1)+!(n-2))

'''
def derangement(n):
    if n == 0:
        return 1
    elif n == 1:
        return 0
    return (n-1) * (derangement(n-1) + derangement(n-2))


print(derangement(5))
print(derangement(6))
print(derangement(9))
print(derangement(14))
# Given two strings of letters, determine whether the second can 
# ... be made from the first by removing one letter. 
# The remaining letters must stay in the same order.
#
# EXAMPLES:
#   funnel("leave", "eave") => true
#   funnel("reset", "rest") => true
#   funnel("dragoon", "dragon") => true
#   funnel("eave", "leave") => false
#   funnel("sleet", "lets") => false

def funnel(string1, string2):
    if len(string2) != (len(string1) - 1):
        return False
    
    list1, list2 = list(string1), list(string2)

    for i in range(len(list1)):
        if list1[:i] + list1[i+1:] == list2:
            return True
    return False

print(funnel("leave", "eave"))
print(funnel("reset", "rest"))
print(funnel("dragoon", "dragon"))
print(funnel("eave", "leave"))
print(funnel("sleet", "lets"))
print(funnel("skiff", "ski"))
